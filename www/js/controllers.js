angular.module('randersfc.controllers', [])

/*.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

// With the new view caching in Ionic, Controllers are only called
// when they are recreated or on app start, instead of every page change.
// To listen for when this page is active (for example, to refresh data),
// listen for the $ionicView.enter event:
//$scope.$on('$ionicView.enter', function(e) {
//});

// Form data for the login modal
$scope.loginData = {};

// Create the login modal that we will use later
$ionicModal.fromTemplateUrl('templates/login.html', {
scope: $scope
}).then(function(modal) {
$scope.modal = modal;
});

// Triggered in the login modal to close it
$scope.closeLogin = function() {
$scope.modal.hide();
};

// Open the login modal
$scope.login = function() {
$scope.modal.show();
};

// Perform the login action when the user submits the login form
$scope.doLogin = function() {
console.log('Doing login', $scope.loginData);

// Simulate a login delay. Remove this and replace with your login
// code if using a login system
$timeout(function() {
$scope.closeLogin();
}, 1000);
};
})*/
.controller('LeftMenuCtrl', function(ApiEndpoint, $ionicLoading, $ionicModal, $rootScope, $q, FeedService, $cordovaAppAvailability, $timeout, $cordovaInAppBrowser, $scope, $location, $state) {

	$scope.closePushMessage = function(){
		$rootScope.pushMessage = null;
	},
	$scope.getLeftMenu = function(){
		var deferred = $q.defer();
		var url = ApiEndpoint.url+"/app/image/sidemenu";
		FeedService.get(url)
		.then(function successCallback(res){
			deferred.resolve();
			$rootScope.sidemenu = res.data.sidemenu;

		}, function errorCallback(res){
			deferred.resolve();
			console.log("Error parsing or getting data from api");
		})
		return deferred.promise;
	},
	$scope.isItemActive = function(url){
		return $location.path().indexOf(url) > 1;    
	},
	$scope.showInAppBrowser = function(url, type){
		var options = {
			location: 'no',
			toolbar: 'yes',
			enableViewportScale: 'yes',
			toolbarposition: 'top',
			closebuttoncaption: 'Luk'
		};

		$timeout(function(){
			$cordovaInAppBrowser.open(url, type, options);
		});
	},
	$scope.openInApp = function(appname, url){
		switch(appname){
			case 'facebook':
			var iosScheme = 'fb://';
			var iosLink = 'fb://profile/147882701918553';        

			var androidScheme = 'com.facebook.katana';
			var androidLink = 'fb://page/147882701918553';  
			break;
			case 'twitter':
			var iosScheme = 'twitter://';
			var iosLink = 'twitter://user?id=329642015';        

			var androidScheme = 'com.twitter.android';
			var androidLink = 'twitter://user?id=329642015';  

			break;
			case 'instagram':
			var iosScheme = 'instagram://';        
			var iosLink = 'instagram://user?username=randersfc';        

			var androidScheme = 'com.instagram.android';
			var androidLink = 'instagram://user?username=randersfc';  

			break;

		}

		if(ionic.Platform.isIOS()){
			$cordovaAppAvailability.check(iosScheme).then(function(){
				$scope.showInAppBrowser(iosLink, '_system');
			}, function(){
				$scope.showInAppBrowser(url, '_blank');
			}
			);
		}
		else{
			$cordovaAppAvailability.check(androidScheme)
			.then(function(){
				$scope.showInAppBrowser(androidLink, '_system');
			}, function(){
				$scope.showInAppBrowser(url, '_blank');
			});
		}
	},
		$scope.showTerms = function() {

		$ionicLoading.show();

		FeedService.get(ApiEndpoint.url+'/app/terms')
		.then(function successCallback(res){
			$ionicLoading.hide();
			$scope.terms = res.data.terms;
			$scope.showTermsModal();

		}, function errorCallback(res){
			$ionicLoading.hide();

			console.log("Error parsing or getting data from api");
		})

	},

	$scope.showTermsModal = function(){

				var url = 'templates/user/_terms.html';

			//show modal			
			$ionicModal.fromTemplateUrl(url, {
				scope: $scope

			}).then(function(modal) {
				$scope.modal = modal;
				$scope.modal.show();
			});
			$scope.closeModal = function() {
				$scope.modal.hide();
			};
	  		// Cleanup the modal when we're done with it!
			$scope.$on('$destroy', function() {
				$scope.modal.remove();
			});
	}


	$scope.getLeftMenu();

})

.controller('HomeCtrl', function($filter, ApiEndpoint, $ionicPopup, $scope, $state, FeedService, $ionicLoading, $rootScope, $q) {
	var numberOfLatestNews = 3;
	$rootScope.lineup = null;
	
	// $scope.getMOTM = function(){
	// 	var deferred = $q.defer();
	// 	var url = ApiEndpoint.url+"/check_motm.php";
	// 	FeedService.get(url)
	// 	.then(function successCallback(res){
	// 		$rootScope.motm = res.data.motm;

	// 		$rootScope.motm.clienttime = $filter('date')(Date(), 'short');
	// 		deferred.resolve();
	// 	}, function errorCallback(res){
	// 		deferred.resolve();
	// 		console.log("Error parsing or getting carrousel-data from api");
	// 	})
	// 	return deferred.promise;
	// },
	$scope.getFanzoneImage = function(){
			var deferred = $q.defer();
			var url = ApiEndpoint.url+"/app/image/fanzone";
			FeedService.get(url)
			.then(function successCallback(res){
				deferred.resolve();

				if(res.data.image){
					$rootScope.fanzoneimage = res.data.image;
				}

			}, function errorCallback(res){
				deferred.resolve();
				console.log("Error parsing or getting data from api");
			})
			return deferred.promise;
		},
	$scope.getCarousel = function(){
		var deferred = $q.defer();
		var url = ApiEndpoint.url+"/app/images/carousel";
		FeedService.get(url)
		.then(function successCallback(res){
			deferred.resolve();

			if(res.data.carousel && res.data.carousel.randomize == 1){
				var length = res.data.carousel.slides.length;
				res.data.carousel.start_index = Math.floor((Math.random() * length) );
			}
			$rootScope.carousel = res.data.images;

		}, function errorCallback(res){
			deferred.resolve();
			console.log("Error parsing or getting data from api");
		})
		return deferred.promise;
	},
	// $scope.getWidgets = function(){
	// 	var deferred = $q.defer();
	// 	var url = ApiEndpoint.url+"/app/widgets/frontpage";
	// 	FeedService.get(url)
	// 	.then(function successCallback(res){
	// 		$rootScope.widgets = res.data.widgets;
	// 		deferred.resolve();

	// 	}, function errorCallback(res){
	// 		deferred.resolve();
	// 		("Error parsing or getting widget-data from api");
	// 	})
	// 	return deferred.promise;
	// },
	$scope.getFeatured = function(){
		var deferred = $q.defer();
		var url = ApiEndpoint.url+"/app/image/featured";
		FeedService.get(url)
		.then(function successCallback(res){
			deferred.resolve();
			$scope.featured = res.data.featured;

		}, function errorCallback(res){
			deferred.resolve();
			console.log("Error parsing or getting featured-data from api");
		})
		return deferred.promise;
	},
	// $scope.getNextMatch = function(){
	// 	var deferred = $q.defer();
	// 	var url = ApiEndpoint.rfc_url+"/kamp.php";
	// 	FeedService.get(url)
	// 	.then(function successCallback(res){
	// 		$scope.nextmatch = res.data.matches[0];
	// 		deferred.resolve();

	// 	}, function errorCallback(res){
	// 		deferred.resolve();
	// 		console.log("Error parsing or getting data from randersfc");
	// 	})
	// 	return deferred.promise;
	// },
	$scope.getTopElement = function(){
		var deferred = $q.defer();
		var url = ApiEndpoint.url+"/app/image/countdown";
		FeedService.get(url)
		.then(function successCallback(res){
			$scope.countdownElement = res.data.image;
			if($scope.countdownElement && $scope.countdownElement.countdown_date){
				$scope.countdownElement.starttime = moment($scope.countdownElement.countdown_date+' '+$scope.countdownElement.countdown_time, 'DD/MM/YYYY H:mm');
			}
			deferred.resolve();

		}, function errorCallback(res){
			deferred.resolve();
			console.log("Error parsing or getting data from api");
		})
		return deferred.promise;
	},
	$scope.getLatestNews = function(){
		var deferred = $q.defer();

		var url = ApiEndpoint.rfc_url+"/nyhed.php?fra=0&antal=3";

		FeedService.get(url)
		.then(function successCallback(res){

			$rootScope.latestnews = [];
			
			for(i = 0; i <= numberOfLatestNews-1; i++){
				$rootScope.latestnews.push(res.data.newsfeed.news[i]);
			}

			if($rootScope.news){
				for(i = 0; i <= numberOfLatestNews-1; i++){
					if(JSON.stringify($rootScope.news[i].title+$rootScope.news[i].pubDate ) != JSON.stringify(res.data.newsfeed.news[i].title+res.data.newsfeed.news[i].pubDate)){
						console.log(res.data.newsfeed.news[i]);
						$rootScope.news.unshift(res.data.newsfeed.news[i]);
					}
				}
			}
			else{
				$rootScope.news = res.data.newsfeed.news;
			}
			deferred.resolve();

		}, function errorCallback(res){
			deferred.resolve();
			$ionicLoading.hide();
			$scope.$broadcast('scroll.refreshComplete');
			$ionicPopup.alert({
				title: 'Der opstod en fejl!',
				content: 'Der kunne ikke hentes data'
			});

			console.log("Error parsing or getting data from randersfc");

		})
		return deferred.promise;
	},
	$scope.getLatestVideo = function(){
		var deferred = $q.defer();

		var url = "https://www.googleapis.com/youtube/v3/playlistItems";

		FeedService.parseYoutubeChannel(url)
		.then(function successCallback(res){
			/*set data from youtube */
			/*$rootScope.latestvideo = res.data.items[0]; */

			if($rootScope.videos){
				if(JSON.stringify($rootScope.videos[0].snippet.title+$rootScope.videos[0].snippet.publishedAt ) != JSON.stringify(res.data.items[0].snippet.title+res.data.items[0].snippet.publishedAt)){
					$rootScope.videos.unshift(res.data.items[0]);
				}
			}
			else{
				$rootScope.videos = res.data.items;
			}
			deferred.resolve();

		}, function errorCallback(res){
			deferred.resolve();

		})
		return deferred.promise;
	},
	$scope.doItAll = function(){

		var deferred = $q.defer();

		/* var promises = [$scope.getLatestNews(), $scope.getLatestVideo(), $scope.getFeatured(), $scope.getTopElement()]; */
		var promises = [$scope.getLatestNews(), $scope.getFeatured(), $scope.getTopElement(), $scope.getLeftMenu(), /*$scope.getWidgets(),*/ $scope.getCarousel(), $scope.getFanzoneImage()];
		return $q.all(promises);

	},
	$scope.doRefresh = function(loading){
		if(loading && !$rootScope.latestnews){
			$scope.isLoadingContent = true;
			// $ionicLoading.show();
		}
		$scope.doItAll().then(function(){
			$scope.isLoadingContent = false;
			$scope.$broadcast('scroll.refreshComplete');
			// $ionicLoading.hide();

			/* Check if man of the match is available */
			// if($rootScope.motm){
			// 	// var now = $filter('date')(Date(), 'short');
			// 	// var motmEnd = $filter('date')($rootScope.motm.endtime, 'short');
			// 	var now = new Date();
			// 	var motmEnd = moment($rootScope.motm.endtime);

			// 	var servertime = moment($rootScope.motm.servertime);				
			// 	var clienttime = moment($rootScope.motm.clienttime);
			// 	var clientServerDiff = servertime.diff(clienttime);
				
			// 	var timeDiff = motmEnd.diff(now);
			// 	console.log(timeDiff);

			// 	if(timeDiff > 0){
			// 		$rootScope.showMOTM = true;
			// 	}
			// }
			// if($scope.frontpageElement && $scope.frontpageElement.showLineup && $scope.frontpageElement.timerDatetime){
			// 	var matchStart = moment($scope.frontpageElement.timerDatetime);
			// 	var now = new Date();
			// 	var timeDiff = matchStart.diff(now);
			// 	var interval = 1000*60*60 /* 1 hour */

			// 	if(timeDiff < interval && timeDiff > -interval){ /* if less than 1 hour to matchstart - check for lineup */
			// 		var lineupUrl = ApiEndpoint.url+"/lineup.php"
			// 		FeedService.get(lineupUrl).then(function(res){
			// 			$rootScope.lineup = res.data.lineup;
			// 		})
			// 	}
			// 	else{
			// 		$rootScope.lineup = null;
			// 	}
			// }


		})
	}

	$scope.doRefresh(true);
})

.controller('NewslistCtrl', function(ApiEndpoint, $cordovaInAppBrowser, $timeout, $scope, FeedService, $ionicLoading, $rootScope, $ionicPopup, $q) {
	var numberOfLatestNews = 3;
	!$rootScope.news ? $rootScope.news = [] : null;
	var fra = 0;
	var antal = 10;
	$scope.newsLoad = false;

	$scope.doRefresh = function(loading){
		return $q(function(resolve, reject){
			if(loading && !$rootScope.latestnews){
				$scope.isLoadingContent = true;

			}
			var url = ApiEndpoint.rfc_url+"/nyhed.php?fra=0&antal=10";
			FeedService.get(url)
			.then(function successCallback(res){
				fra = 10;
				$rootScope.news = res.data.newsfeed.news;


				$rootScope.latestnews = [];
				for(i = 0; i <= numberOfLatestNews-1; i++){
					$rootScope.latestnews[i] = res.data.newsfeed.news[i];
				}

				/* hide loading spinner */
				$ionicLoading.hide();
				$scope.$broadcast('scroll.refreshComplete');
				resolve();

			}, function errorCallback(res){
				console.log("Error parsing or getting data from randersfc");
				$scope.$broadcast('scroll.refreshComplete');
				$ionicLoading.hide();
				$ionicPopup.alert({
					title: 'Der opstod en fejl!',
					content: 'Der kunne ikke hentes data'
				});

				reject();

			})
		})
	},
	$scope.loadMore = function(){
		var url = ApiEndpoint.rfc_url+"/nyhed.php?fra="+fra+"&antal="+antal;

		if($rootScope.news){
			FeedService.get(url)
			.then(function(res){
				fra = fra + 10;
				$rootScope.news.push.apply($rootScope.news, res.data.newsfeed.news);
				$scope.$broadcast('scroll.infiniteScrollComplete');
			});
		}
	}
	/* show loading spinner when the page loads */
	//$ionicLoading.show();
	/* do initial pagerefresh when page loads. */
	$scope.doRefresh(true).then(function(){
		$scope.isLoadingContent = false;
		$timeout(function(){
			$scope.newsLoad = true;
		}, 1000)
	})
})  

.controller('NewsCtrl', function($scope, $state, $rootScope) {
	$scope.newsitem = $rootScope.news[$state.params.newsIndex];
})

.controller('TvCtrl', function($scope, $rootScope, $stateParams, $http, $ionicLoading, FeedService, $ionicPopup, $timeout) {
	$rootScope.videos ? null : $rootScope.videos = [];

	$scope.doRefresh = function(){
		var url = "https://www.googleapis.com/youtube/v3/playlistItems";

		FeedService.parseYoutubeChannel(url)
		.then(function successCallback(res){
			/* set data from youtube */
			$rootScope.videos = res.data.items;
			$rootScope.latestvideo = res.data.items[0];
			/* set token for next page */
			res.data.nextPageToken ? $scope.nextpage = res.data.nextPageToken : $scope.nextpage = false;

			/* hide loading spinner */
			$ionicLoading.hide();
			$scope.$broadcast('scroll.refreshComplete');

			$scope.loadMore = function(){

				var length = $scope.videos.length;
				var position = $scope.videos[length-1].snippet.position;

				var url = "https://www.googleapis.com/youtube/v3/playlistItems";

				FeedService.parseMoreYoutubeChannel(url, $scope.nextpage).then(function(res){
					$rootScope.videos.push.apply($rootScope.videos, res.data.items);
					/* set token for next page */
					res.data.nextPageToken ? $scope.nextpage = res.data.nextPageToken : $scope.nextpage = false;
					$scope.$broadcast('scroll.infiniteScrollComplete');

				});
			}

		}, function errorCallback(res){
			/* Stop the ion-refresher from spinning */
			$ionicLoading.hide();
			$scope.$broadcast('scroll.refreshComplete');
			$ionicPopup.alert({
				title: 'Der opstod en fejl!',
				content: 'Der kunne ikke hentes data fra youtube'
			});
		})
	}

	/* show loading spinner when the page loads */
	$ionicLoading.show();
	/* do initial pagerefresh when page loads. */
	$scope.doRefresh();

})

.controller('VideoCtrl', function($scope, $rootScope, $state, $stateParams) {
	$scope.video = $rootScope.videos[$stateParams.videoIndex];
	$scope.videosrc = 'https://youtube.com/embed/'+$scope.video.snippet.resourceId.videoId+'?rel=0&controls=1&modestbranding=1&showinfo=0';
	$scope.videoid = $scope.video.snippet.resourceId.videoId;
})

.controller('MatchesCtrl', function(ApiEndpoint, $ionicPopup, $scope, $state, FeedService, $ionicLoading) {
	$scope.doRefresh = function(loading){
		if(loading){
			$scope.isLoadingContent = true;
		}
	
		var url = ApiEndpoint.rfc_url+"/kamp.php?"+Date.now();
		FeedService.get(url)
		.then(function successCallback(res){
			/*$scope.nextmatch = res.data.matches[0]; */
			/*res.data.matches.splice(0, 1); */
			$scope.matches = res.data.matches;

			/*hide loading spinner */
			$scope.isLoadingContent = false;
			$ionicLoading.hide();
			$scope.$broadcast('scroll.refreshComplete');

		}, function errorCallback(res){
			console.log("Error parsing or getting data from randersfc");
			/* Stop the ion-refresher from spinning */
			$ionicLoading.hide();
			$scope.isLoadingContent = false;
			$scope.$broadcast('scroll.refreshComplete');
			$ionicPopup.alert({
				title: 'Der opstod en fejl!',
				content: 'Der kunne ikke hentes data'
			});
		})
	}

	/* show loading spinner when the page loads */
	//$ionicLoading.show();
	/*do initial pagerefresh when page loads. */
	$scope.doRefresh(true); 
})

.controller('ResultsCtrl', function(ApiEndpoint, $ionicPopup, $scope, $state, FeedService, $ionicLoading) {
	$scope.doRefresh = function(loading){
		if(loading){
			$scope.isLoadingContent = true;
		}
		var url = ApiEndpoint.rfc_url+"/resultater.php?"+Date.now();
		FeedService.get(url)
		.then(function successCallback(res){
			/*$scope.nextmatch = res.data.matches[0]; */
			/*res.data.matches.splice(0, 1); */
			$scope.results = res.data.matches;

			/*hide loading spinner */
			$scope.isLoadingContent = false;
			$ionicLoading.hide();
			$scope.$broadcast('scroll.refreshComplete');

		}, function errorCallback(res){
			console.log("Error parsing or getting data from randersfc");
			/* Stop the ion-refresher from spinning */
			$ionicLoading.hide();
			$scope.isLoadingContent = false;
			$scope.$broadcast('scroll.refreshComplete');
			$ionicPopup.alert({
				title: 'Der opstod en fejl!',
				content: 'Der kunne ikke hentes data'
			});
		})
	}

	/* show loading spinner when the page loads */
	//$ionicLoading.show();
	/*do initial pagerefresh when page loads. */
	$scope.doRefresh(true); 
})


.controller('SettingsCtrl', function(ApiEndpoint, FeedService, $scope, $rootScope, $state, $stateParams, myStorage) {

	$scope.regid = myStorage.get('regid');
	$scope.isLoadingContent = true;

	// $scope.notificationTypes = [
	// { id: 1, text: 'Konkurrencer', checked: true},
	// { id: 2, text: 'Nyheder', checked: true},
	// { id: 3, text: 'Gode tilbud', checked: true}
	// ];

	// var type1 = myStorage.get('notificationType=1') != 0 ? $scope.notificationTypes[0].checked = true : $scope.notificationTypes[0].checked = false;
	// var type2 = myStorage.get('notificationType=2') != 0 ? $scope.notificationTypes[1].checked = true : $scope.notificationTypes[1].checked = false;
	// var type3 = myStorage.get('notificationType=3') != 0 ? $scope.notificationTypes[2].checked = true : $scope.notificationTypes[2].checked = false;


	$scope.changeNotification = function(item){

		var data = {};
		data.regid = $scope.regid;
		data.type_id = item.id;

		var url = ApiEndpoint.url+"/app/notifications/change";
		FeedService.post(url, data)
		.then(function successCallback(res){
			// myStorage.set('notificationType='+item.id, value);
		})
	},
	$scope.getNotifications = function(){
		console.log('getting notifications');
		$scope.regid = myStorage.get('regid');
		if($scope.regid){

			var url = ApiEndpoint.url+"/app/notifications";
			var data = {};
			data.regid = $scope.regid;

			FeedService.post(url, data)
			.then(function successCallback(res){
				$scope.$broadcast('scroll.refreshComplete');
				$rootScope.notificationTypes = res.data.types;
				$scope.isLoadingContent = false;
				// myStorage.set('notificationType='+item.id, value);
			})
		}
		else{
			$scope.isLoadingContent = false;
			$scope.$broadcast('scroll.refreshComplete');
		}
	}
	if($scope.isLoadingContent){
		$scope.getNotifications();
	}

})

.controller('PositionsCtrl', function(ApiEndpoint, $scope, $state, myStorage, FeedService, $ionicLoading, $ionicModal, $ionicPopup) {
	$scope.doRefresh = function(loading){
		if(loading){
			$scope.isLoadingContent = true;
		}
		var url = ApiEndpoint.rfc_url+"/stillingnew.php?"+Date.now();
		FeedService.get(url)
		.then(function successCallback(res){
			console.log(res.data);
			$scope.positions = res.data;

			/*hide loading spinner*/
			$scope.isLoadingContent = false;
			$ionicLoading.hide();
			$scope.$broadcast('scroll.refreshComplete');

		}, function errorCallback(res){
			console.log("Error parsing or getting data from randersfc");
			/* Stop the ion-refresher from spinning */
			$ionicLoading.hide();
			$scope.isLoadingContent = false;
			$scope.$broadcast('scroll.refreshComplete');
			$ionicPopup.alert({
				title: 'Der opstod en fejl!',
				content: 'Der kunne ikke hentes data fra RandersFC.dk'
			});
		})
	},
	$scope.triggerModal = function(templateUrl, item){
		$scope.selectedItem = item;
		/* Create the login modal that we will use later */
		$ionicModal.fromTemplateUrl('templates/'+templateUrl, {
			scope: $scope,
		})
		.then(function(modal) {
			$scope.modal = modal;

			/* Triggered to close modal*/
			$scope.closeModal = function() {
				$scope.modal.hide();
			};

			/* Open the modal */
			$scope.openModal = function() {
				$scope.modal.show();
			}; 

			/* Show modal */
			$scope.openModal();
		});
	}

	/* show loading spinner when the page loads */
	//$ionicLoading.show();
	/* do initial pagerefresh when page loads. */
	$scope.doRefresh(true);

})

.controller('QuizStartCtrl', function(UserService, ApiEndpoint, $scope, $ionicHistory, $location, Flash, FeedService, myStorage, $ionicLoading, $rootScope, $state, $stateParams, myStorage) {
	
	$scope.$on('$ionicView.enter', function(e) {
			$scope.user = myStorage.getObj('currentUser');

		//CHECK FACEBOOK INFO
		// console.log("check user for changes");
		// UserService.checkUserForChanges()
		// .then(function successCallback(user){
		// 	$scope.user = user;
		// 	if(!$scope.user.picture){
		// 		$scope.user.picture = "img/avatar-"+$scope.user.gender+".png";
		// 	}
		// })
	});

		

	if(myStorage.get('activeQuiz')){
		console.log("removing active quiz");
		myStorage.remove('activeQuiz');
	}

	$scope.submitDisabled = true;


	$scope.checkCode = function(code){
		if(code.length == 5 ){
			$scope.submitDisabled = false;
		}
		else{
			$scope.submitDisabled = true; 
		}

	},
	$scope.getQuiz = function(code){
		$ionicLoading.show();

		// var data = {'token' : $scope.user.token, 'quiz_code' : code};

		FeedService.get(ApiEndpoint.url+"/quiz/"+code)
		.then(function successCallback(res){
			$ionicLoading.hide();
			console.log(res);
			if(res.data.token_accepted){
				if(res.data.result == 'success'){
					Flash.clear();
					var activeQuiz = res.data;
					activeQuiz.quiz_code = code;

					myStorage.setObj('activeQuiz', activeQuiz);
					$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
					$state.go('app.quizroom.countdown');
				}
				else{
					Flash.clear();
					Flash.create('danger', res.data.msg, 5000, null, true);
				}

			}
			else{

				myStorage.remove('currentUser');
				$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
				$location.path('/app/fanzone/user-login');

			}

		});

	}


})



.controller('UserLoginCtrl', function(ApiEndpoint, $ionicModal, $scope, $cordovaFacebook, $location, $ionicHistory, Flash, FeedService, $ionicLoading, $rootScope, $state, $stateParams, myStorage) {

	Flash.clear();
	$scope.user = {};
	$scope.doFBLogin = function(){
		$cordovaFacebook.login(["public_profile", "email"])
		.then(function(success){

			var fbUser = {};
			fbUser.accessToken = success.authResponse.accessToken;
			fbUser.userID = success.authResponse.userID;

			$cordovaFacebook.api("/me?fields=id,first_name,last_name,name,email,picture.width(720)")
			.then(function(success){
				// fbUser.gender = success.gender;
				fbUser.email = success.email;
				success.picture.data.is_silhouette ? fbUser.picture = null : fbUser.picture = success.picture.data.url;

				fbUser.name = success.name;
				fbUser.first_name = success.first_name;
				fbUser.last_name = success.last_name;
				fbUser.regid = myStorage.get('regid');
				fbUser.device = $rootScope.deviceOS;

				/*save fb user to db */
				$ionicLoading.show();
				FeedService.post(ApiEndpoint.url+"/contestants/facebook/login", fbUser)
				.then(function successCallback(res){
					$ionicLoading.hide();

					if(res.data.result == 'success'){
						Flash.clear();

						fbUser.token = res.data.token;
						fbUser.user_id = res.data.user_id;
						myStorage.setObj('currentUser', fbUser);
						$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
						$rootScope.user = fbUser;
						if(!$rootScope.user.picture){
							$rootScope.user.picture = "img/avatar-"+$rootScope.user.gender+".png";
						}
						$location.path('app/fanzone');
					}
					else{
						Flash.clear();
						Flash.create('danger', res.data.msg, 5000, null, true);

					}

				}, function(error){
					$ionicLoading.hide();

					Flash.clear();
					Flash.create('danger', 'Der opstod en fejl i dit login!', 5000, null, true);
				});


			}, function(error){
				Flash.clear();
				Flash.create('danger', 'Der opstod en fejl i forbindelsen til Facebook!', 5000, null, true);
			});
		},
		function(error){
			if(!error == "User cancelled."){
				Flash.clear();
				Flash.create('danger', '<strong>Der opstod en fejl!</strong> Vi kunne ikke oprette forbindelse til Facebook', 5000, null, true);
			}

		});
	},
	$scope.doLogin = function(user){
		if($scope.validateUser(user)){

			$ionicLoading.show();

			var url = ApiEndpoint.url+"/contestants/login";
			var loginUser = angular.copy(user);
			loginUser.password = btoa(loginUser.password);
			loginUser.regid = myStorage.get('regid');
			loginUser.device = $rootScope.deviceOS;


			FeedService.post(url, loginUser)
			.then(function successCallback(res){
				$ionicLoading.hide();
				if(res.data.result == 'success'){
					Flash.clear();

					var user = {};
					user.name = res.data.name;
					user.email = res.data.email;
					user.first_name = res.data.first_name;
					user.gender = res.data.gender;
					user.user_id = res.data.user_id;
					user.token = res.data.token;
					user.picture = res.data.picture;

					myStorage.setObj('currentUser', user);
					$rootScope.user = user;
					if(!$rootScope.user.picture){
						$rootScope.user.picture = "img/avatar-"+$rootScope.user.gender+".png";
					}
					$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
					$location.path('app/fanzone');

				}
				else{
					Flash.clear();
					Flash.create('danger', res.data.msg, 5000, null, true);
				}
			});
		}
	},
	$scope.validateUser = function(user){
		var error = false;

		if(!user.email || user.email == '' || user.email == undefined ){
			error = 'Du mangler at indtaste en e-mail';
		}
		else if(!user.password || user.password == '' || user.password == undefined){
			error = 'Du mangler at indtaste et password';
		}

		if(error){
			Flash.create('danger', error, 5000, null, true);
			return false;
		}
		else{
			return true
		}  
	}


})



.controller('CreateUserCtrl', function(ApiEndpoint, $ionicModal, $ionicHistory, $location, Flash, $scope, FeedService, $ionicLoading, $rootScope, $state, $stateParams, myStorage) {
	Flash.clear();
	$scope.user = {};
	$scope.user.gender = 'male';
	$scope.user.acceptTerms = false;

	$scope.errorShow = false;
	$scope.errorText = '';

	$scope.submitUser = function(user){
		if($scope.validateUser(user)){
			$ionicLoading.show();

			var url = ApiEndpoint.url+"/contestants/create";
			var createUser = angular.copy(user);
			createUser.password = btoa(createUser.password);
			createUser.first_name = createUser.name.split(' ')[0];
			createUser.regid = myStorage.get('regid');
			createUser.device = $rootScope.deviceOS;

			FeedService.post(url, createUser)
			.then(function successCallback(res){
				$ionicLoading.hide();
				if(res.data.result == 'success'){
					$scope.errorShow = false;
					createUser.token = res.data.token;
					createUser.user_id = res.data.user_id;

					delete createUser.password; 
					delete createUser.repassword;
					myStorage.setObj('currentUser', createUser);

					$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
					$location.path('app/fanzone');

					/*$ionicHistory.goBack(-1); */
				}
				else{
					$scope.errorShow = true;
					$scope.errorText = res.data.msg;
				}
			}, function errorCallback(res){
				$ionicLoading.hide();
				console.log("Error parsing or getting data from api");
			})
		}
	},
	$scope.validateUser = function(user){
		var error = false;
		if(!user.name || user.name == '' || user.name == undefined){
			error = 'Indtast dit navn';
		}
		else if(!user.email || user.email == '' || user.email == undefined || user.email.indexOf('@') == -1 || user.email.indexOf('.') == -1){
			error = 'Indtast en gyldig email';
		}
		else if(user.gender != 'male' && user.gender !== 'female'){
			error = 'Vælg dit køn';
		}
		else if(!user.password || user.password == '' || user.password == undefined){
			error = 'Indtast et password';
		}
		else if(!user.repassword || user.repassword == '' || user.repassword == undefined){
			error = "Du skal gentage dit password";
		}
		else if(user.password != user.repassword){
			error = "Du har angivet to forskellige passwords";
		}
		else if(!user.acceptTerms){
			console.log(user.acceptTerms);
			error = "Du skal acceptere vores betingelser!";
		}

		if(error){
			$scope.errorShow = true;
			$scope.errorText = error;
		}
		else{
			$scope.errorShow = false;
			return true
		}
	}





})

.controller('MyProfileCtrl', function(UserService, ApiEndpoint, $scope, $cordovaFacebook, $location, $ionicHistory, Flash, FeedService, $ionicLoading, $rootScope, $state, $stateParams, myStorage) {

	Flash.clear();

	UserService.checkUserForChanges()
	.then(function successCallback(user){
		if(user){
			$scope.user = user;
			/* Set placeholder if user has no picture */
			if(!$scope.user.picture){
				$scope.user.picture = "img/avatar-"+$scope.user.gender+".png";
			}
		}
	})
	// $scope.user = myStorage.getObj('currentUser');

	$scope.logout = function(){
		myStorage.remove('currentUser');
		$cordovaFacebook.logout()
		.then(function(success){
			console.log("log out success");
		}, function(error){
			console.log("log out error");
		});

		$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
		$location.path('/app/fanzone/user-login');
	},
	$scope.updateUser = function(user){
		$ionicLoading.show();
		var url = ApiEndpoint.url+"/contestant/update";
		FeedService.post(url, user)
		.then(function successCallback(res){
			$ionicLoading.hide();
			if(res.data.token_accepted){
				if(res.data.result == 'success'){
					myStorage.setObj('currentUser', user);
				}
				else{
					Flash.clear();
					Flash.create('danger', res.data.msg, 5000, null, true);
				}
			}
			else{

				myStorage.remove('currentUser');
				$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
				$location.path('/app/fanzone/user-login');

			}
		}, function errorCallback(res){
			$ionicLoading.hide();
			console.log("Error parsing or getting data from api");
		})
	};

	/* notifications */
	$scope.regid = myStorage.get('regid');

	$scope.notificationTypes = [
	{ id: 1, text: 'Konkurrencer', checked: true},
	{ id: 2, text: 'Nyheder', checked: true},
	{ id: 3, text: 'Gode tilbud', checked: true}
	];

	var type1 = myStorage.get('notificationType=1') != 0 ? $scope.notificationTypes[0].checked = true : $scope.notificationTypes[0].checked = false;
	var type2 = myStorage.get('notificationType=2') != 0 ? $scope.notificationTypes[1].checked = true : $scope.notificationTypes[1].checked = false;
	var type3 = myStorage.get('notificationType=3') != 0 ? $scope.notificationTypes[2].checked = true : $scope.notificationTypes[2].checked = false;


	$scope.changeNotification = function(item){
		var value;
		item.checked ? value = 1: value = 0;  


		var url = ApiEndpoint.url+"/app/notifications/change?regid="+$scope.regid+"&notification_id="+item.id+"&notification_value="+value;
		FeedService.get(url)
		.then(function successCallback(res){
			myStorage.set('notificationType='+item.id, value);
		})
	}
/* /notifications */

})

.controller('ForgotPasswordCtrl', function(ApiEndpoint, $scope, $location, $ionicHistory, Flash, FeedService, $ionicLoading, $rootScope, $state, $stateParams, myStorage) {
	Flash.clear();
	$scope.email = "";

	$scope.resetPassword = function(email){
		var error = false;
		//validate email
		if(!email || email == '' || email == undefined || email.indexOf('@') == -1 || email.indexOf('.') == -1){
			error = 'Indtast en gyldig email';
		}

		//check if any errors
		if(error){
			Flash.clear();
			Flash.create('danger', error, 5000, null, true);

			$scope.errorText = error;
		}
		else{

			$ionicLoading.show();
			var url = ApiEndpoint.url+"/contestants/password/forgot";

			FeedService.post(url, {'email' : email})
			.then(function successCallback(res){
				$ionicLoading.hide();
				if(res.data.result == 'success'){

					$location.path('/app/fanzone/reset-password');

				}
				else{
					Flash.clear();
					Flash.create('danger', res.data.msg, 5000, null, true);
				}

			}, function errorCallback(res){
				$ionicLoading.hide();
				console.log("Error parsing or getting data from api");
			})
		}

	},
	$scope.enterActivationCode = function(code){
		var error = false;

		if(!code || code == ''){
			error = 'Indtast din aktiverings kode';
		}

		if(error){
			Flash.clear();
			Flash.create('danger', error, 5000, null, true);
		}
		else{
			$ionicLoading.show();
			var url = ApiEndpoint.url+"/contestants/password/token";

			FeedService.post(url, {'token' : code})
			.then(function successCallback(res){
				$ionicLoading.hide();

				if(res.data.result == 'success'){
					//save token
					myStorage.set('forgot_password_token', code);
					$location.path('/app/fanzone/pick-new-password');

				}
				else{
					Flash.clear();
					Flash.create('danger', res.data.msg, 5000, null, true);
				}

			}, function errorCallback(res){
				$ionicLoading.hide();
				console.log("Error parsing or getting data from api");
			})

		}

	},
	$scope.saveNewPassword = function(pwObj){
		var error = false;

		if(!pwObj.password || pwObj.password == '' || pwObj.password == undefined){
			error = 'Indtast et password';
		}
		else if(!pwObj.repassword || pwObj.repassword == '' || pwObj.repassword == undefined){
			error = "Du skal gentage dit password";
		}
		else if(pwObj.password != pwObj.repassword){
			error = "Du har angivet to forskellige passwords";
		}

		if(error){
			Flash.clear();
			Flash.create('danger', error, 5000, null, true);
		}
		else{
			$ionicLoading.show();
			var code = myStorage.get('forgot_password_token');
			var password = btoa(pwObj.password);
			var url = ApiEndpoint.url+"/contestants/password/reset";
			FeedService.post(url, {'token' : code, 'password' : password})
			.then(function successCallback(res){
				$ionicLoading.hide();

				if(res.data.result == 'success'){
					//save token
					myStorage.remove('forgot_password_token');
					$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
					Flash.create('success', res.data.msg, 5000, null, true);
					$location.path('/app/fanzone/user-do-login');

				}
				else{
					Flash.clear();
					Flash.create('danger', res.data.msg, 5000, null, true);
				}

			}, function errorCallback(res){
				$ionicLoading.hide();
				console.log("Error parsing or getting data from api");
			})

		}

	}

})


.controller('QuizCtrl', function($ionicPlatform, $ionicScrollDelegate, SocketService, $cordovaKeyboard, $ionicModal, $rootScope, $scope, $location, $ionicHistory, Flash, FeedService, $ionicLoading, $rootScope, $state, $stateParams, myStorage) {
	//var stats = {};
	//stats.top3 = [{'name' : 'Steffen', 'answer' : '1234'}, {'name' : 'Steffen', 'answer' : '321'}, {'name' : 'Steffen', 'answer' : '1234'}];
	//$scope.stats = stats;
	$scope.tempIntAnswer = {'answer' : ''};
	$scope.currentIntAnswer = '';
	$scope.sseconds = '-';
	$scope.mminutes = '-';
	$scope.currentQuestion = {};
	$scope.animate = false;
	$scope.showCorrectAnswer = false;
	$scope.currentAnswer = null;
	$scope.timeDiff = 0;
	$scope.hasPinged = false;
	$scope.testtimer = Date.now() + 18000
	$scope.quiz = myStorage.getObj('activeQuiz');

	$scope.currentUser = myStorage.getObj('currentUser');

	//Keep device from sleeping  while in quiz
	if(window.plugins){
		window.plugins.insomnia.keepAwake();	
	}

	// //If user uses the power button to sleep
	var resumeListener = $ionicPlatform.on('resume', function(){
		console.log("resuming");
  	 	SocketService.connect();
  	 	$scope.ping();
	 	$scope.enterQuiz($scope.quiz.quiz_code);
	});

    $scope.$on('$destroy', function() {
        resumeListener();
    });

	$scope.$on('$ionicView.enter', function(e) {
		SocketService.connect();
		console.log('connected', SocketService);
		$scope.ping();
	 	$scope.enterQuiz($scope.quiz.quiz_code);
 		$scope.animate = true;
	});

	$scope.$on('$ionicView.leave', function(e) {
		SocketService.removeAllListeners();
		SocketService.disconnect();

		window.plugins.insomnia.allowSleepAgain();
	});
	$scope.getCirclePct = function(endtimer){
		var now = Date.now();
		if(!$rootScope.circlestart){
			$rootScope.circlestart = now;
		}
		var current = (endtimer - now)/1000; 
		var timeLeft = (endtimer - $rootScope.circlestart)/1000;
		var newVal = Math.round((current/timeLeft)*100);
		return newVal;
	},	
	$scope.countdown = function(endtimer){
		var now = Date.now();
		var current = Math.round((endtimer - now)/1000); 
		return current;
	},
	$scope.shouldFlashCountdown = function(timer, breakpoint){
		var now = Date.now();
		if((timer-now <= breakpoint && timer-now > 0)){
			console.log("true");
			return true;
		}
		else{
			console.log("false");
			return false;
		}
	},
	$scope.enterQuiz = function(quiz_code){
 		var enterData = {
 			'quiz_code' : quiz_code,
 			'token' 	: $scope.currentUser.token,
 			'name'		: $scope.currentUser.name,
 			'picture'	: $scope.currentUser.picture, 			
 			'gender'	: $scope.currentUser.gender,
 			'user_id'	: $scope.currentUser.user_id
 		}
 		SocketService.emit('join:quiz', enterData);

 	},
	$scope.go = function(state){
		$state.go(state);
	},
	$scope.setCurrentAnswer = function(answer, index){
		if(!$scope.showCorrectAnswer){
			answer.answer_index = index;
			$scope.currentAnswer = answer;
		}
	},
	// $scope.setCurrentIntAnswer = function(answer){
	// 	if(!$scope.showCorrectAnswer){
	// 		answer = answer.replace(/\D/g,'');
	// 		 $scope.currentIntAnswer = answer;
	// 		 $scope.tempIntAnswer.answer = answer;
	// 	}
	// },
	$scope.disconnectFromQuiz = function(){
 		SocketService.removeListener();
		SocketService.disconnect();
	},
	$scope.ping = function(){
		$scope.pingStart = Date.now();
		SocketService.emit('service:ping');	
	},
	$scope.numberWithSeperator = function(x, seperator) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, seperator);
	},
	$scope.pushKey = function(value){
		if(!$scope.showCorrectAnswer){
			switch(value){
				case 'delete_all':
					if($scope.currentIntAnswer){
						$scope.currentIntAnswer = '';
						$scope.tempIntAnswer.answer = '';					
					}
				break;

				case 'delete':
					if($scope.currentIntAnswer){
						$scope.currentIntAnswer = $scope.currentIntAnswer.slice(0, -1);
						$scope.tempIntAnswer.answer = $scope.numberWithSeperator($scope.currentIntAnswer, '.');					
					}

				break;

				default:
				if($scope.currentIntAnswer.length < 15){ //int values can be any longer
			 		$scope.currentIntAnswer += value;
					if($scope.currentIntAnswer.slice(-1) != 0){
						$scope.currentIntAnswer = Number($scope.currentIntAnswer).toString();
					}
					$scope.tempIntAnswer.answer = $scope.numberWithSeperator($scope.currentIntAnswer, '.');	
				}				
				
			}
		}
	}
	// $scope.openModal = function(url) {
	// 	$ionicModal.fromTemplateUrl(url, {
	// 		scope: $scope

	// 	}).then(function(modal) {
	// 		$scope.modal = modal;
	// 		$scope.modal.show();
	// 	});
	// 	$scope.closeModal = function() {
	// 		$scope.modal.hide();
	// 	};
 //  		// Cleanup the modal when we're done with it!
	// 	$scope.$on('$destroy', function() {
	// 		$scope.modal.remove();
	// 	});
	// }
	
 	//join quizroom

 	SocketService.on('service:pong', function(serverTimestamp){
 		var now = Date.now();
 		var clientMs = (now - $scope.pingStart) / 2;
 		var serverTime = serverTimestamp + clientMs;
 		var timeDiff = now - serverTime;
		$scope.hasPinged = true;
 		$scope.timeDiff = Math.round(timeDiff);


 		// var newDate = new Date(new Date($scope.quiz.quiz_starttimestamp) + timeDiff);
 		// console.log("dsads2", newDate);
 		// $scope.countdownTimer = newDate;
 		

	});

	

 	SocketService.on('new:question', function(quizData){
		$scope.showCorrectAnswer = false;

 		$scope.currentAnswer = null;
 		$scope.currentQuestion = quizData;
 		if(quizData.question_type != 1){
			$scope.go('app.quizroom.question');
 		}
 		else{
			$scope.go('app.quizroom.int_question');
 		}
		// $scope.messages.push(msg);
		// $ionicScrollDelegate.scrollBottom();
	});

	SocketService.on('do:submit', function(){
		if(window.cordova && window.cordova.plugins.Keyboard){
			$cordovaKeyboard.close();
		}
		if($scope.currentQuestion.question_type != 1){
			if(!$scope.currentAnswer){
				var submitAnswer = {'token' : $scope.currentUser.token, 'answer_index' : -1,'question_index' :$scope.currentQuestion.index};
				SocketService.emit('submit:answer', submitAnswer);
			}
			else{
				console.log("svar");
				var submitAnswer = {'token' : $scope.currentUser.token, 'answer_index' : $scope.currentAnswer.answer_index,'question_index' :$scope.currentQuestion.index};
				SocketService.emit('submit:answer', submitAnswer);
			}
		}
		else{
			if(!$scope.currentIntAnswer || $scope.currentIntAnswer == ''){
				var submitAnswer = {'token' : $scope.currentUser.token, 'answer' : -1,'question_index' :$scope.currentQuestion.index};
				SocketService.emit('submit:answer', submitAnswer);
			}
			else{
				console.log("svar");
				var submitAnswer = {'token' : $scope.currentUser.token, 'answer' : $scope.currentIntAnswer, 'question_index' :$scope.currentQuestion.index};
				SocketService.emit('submit:answer', submitAnswer);
			}
		}

		$scope.showCorrectAnswer = true;


		//$scope.go('app.quizroom.working');
		// $scope.messages.push(msg);
		// $ionicScrollDelegate.scrollBottom();
	});

 	SocketService.on('show:stats', function(stats){
 		$scope.stats = stats;

 		$state.go('app.quizroom.stats');
	});

 	SocketService.on('show:final-stats', function(stats){
		$rootScope.stats = stats;
		$scope.stats = stats;
		
		//$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
 		$state.go('app.quizroom.final_stats');

	});

 	SocketService.on('show:final-rank', function(){
 		$scope.disconnectFromQuiz();
		$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});

 		$state.go('app.quiz-final_rank');

	});

 	SocketService.on('show:result', function(){
 		$scope.disconnectFromQuiz();
		$rootScope.stats = $scope.stats;

		$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
 		$state.go('app.quiz-result');

	});

 	SocketService.on('no:correct', function(){
 		$scope.disconnectFromQuiz();
		$rootScope.stats = $scope.stats;

		$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
 		$state.go('app.quiz-no_correct');

	});

 	SocketService.on('quiz:loser', function(){
 		$scope.disconnectFromQuiz();
		$rootScope.stats = $scope.stats;

		$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});

 		$state.go('app.quiz-loser');
	});

 	SocketService.on('error:msg', function(error){
 		$scope.disconnectFromQuiz();

		$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
		$rootScope.quizError = error;


 		$state.go('app.quiz-error');
		//$scope.openModal('templates/quiz/_lateAnswer.html');
	});


})

.controller('PrizeListCtrl', function(ApiEndpoint, $scope, $ionicHistory, $location, $rootScope, $state, $stateParams, $ionicLoading, myStorage, FeedService, Flash) {
	$rootScope.prizes = {};
	$scope.isLoadingContent = false;

	$scope.currentUser = myStorage.getObj('currentUser');

	$scope.getPrizes = function(loading){
		if(loading){
			$scope.isLoadingContent = true;
		}
		// $ionicLoading.show();

		var data = {'token' : $scope.currentUser.token};



		FeedService.post(ApiEndpoint.url+'/contestants/prizes', data)
		.then(function successCallback(res){
			$scope.isLoadingContent = false;
			$ionicLoading.hide();
				if(res.data.result == 'success'){

					$rootScope.prizes = res.data.prizes;

				}
				else{
					Flash.clear();
					Flash.create('danger', res.data.msg, 5000, null, true);
				}

		}, function errorCallback(res){
			$scope.isLoadingContent = false;
			$ionicLoading.hide();
			console.log("Error parsing or getting data from api");
		})
	}

	if($scope.currentUser){
		$scope.getPrizes(true);
	}


})
.controller('ShowPrizeCtrl', function(ApiEndpoint, $ionicListDelegate, $ionicPopup, $scope, $ionicHistory, $location, $rootScope, $state, $stateParams, $ionicLoading, myStorage, FeedService, Flash) {

	$scope.prizeIndex = $state.params.prizeIndex;
	$scope.user = myStorage.getObj('currentUser');

	$scope.prize = $rootScope.prizes[$scope.prizeIndex];

	$scope.releasePrize = function(){
		$ionicPopup.confirm({
			title: 'Bekræft udlevering!',
			template: 'Tryk på godkend for at bekræfte at præmien udleveres!',
			buttons: [
				{
					text: 'Annuller', 
					type:'button-assertive', 
					onTap: function(e){
						$ionicListDelegate.closeOptionButtons();
						return false;
					}
				},
				{
					text: 'Bekræft', 
					type:'button-balanced', 
					onTap: function(e){
						return true;
					}
				}
			]
		})
		.then(function(res) {
			if(res) {
				var url = ApiEndpoint.url+"/contestants/prizes/release";
				var data = {'token' : $scope.user.token, 'prize_id' : $scope.prize.id};

				FeedService.post(url, data)
				.then(function successCallback(res){

					Flash.create('success', 'Præmien blev udleveret!', 5000, false, true);

					$ionicHistory.goBack();
					// nextViewOptions({disableBack: true, historyRoot: true});
					// $state.go('app.prize-list');


				}, function errorCallback(res){
					$ionicPopup.alert({
						title: 'Der opstod en fejl!',
						template: 'Udleveringen blev ikke registreret!',
					})
					console.log("Error releasing prize");
				})
			}
		});
	}
})

.controller('QuizResultCtrl', function($scope, $ionicModal, $rootScope, $ionicHistory, $location, $rootScope, $state, $stateParams, $ionicLoading, myStorage, FeedService, Flash) {



	$scope.activeQuiz = myStorage.getObj('activeQuiz');
	$scope.stats = $rootScope.stats;
	$scope.prize = $rootScope.prize;	
	$scope.currentUser = myStorage.getObj('currentUser');
	$scope.myRank = '';

		// $scope.myRank = {};
		// $scope.myRank.gender = "male";
		// $scope.myRank.name = "Steffe";
		// $scope.myRank.rank = 4;
		// $scope.myRank.user_id = 2730;
		// $scope.myRank.token = "81ce0866d1cdbf0b79349171b0644ab0";
		// $scope.myRank.prize = {};
		// $scope.myRank.prize.description = "description";
		// $scope.myRank.prize.id = 13;
		// $scope.myRank.prize.image = "httpfingerkick.jpg";
		// $scope.myRank.prize.quiz_id = 2;
		// $scope.myRank.prize.ranking = 1;
		// $scope.myRank.prize.title = "testtitle";
		// $scope.myRank.prize.winner = "81ce0866d1cdbf0b79349171b0644ab0";

	$scope.getMyRank = function(rankings, token){
		for (var i = 0, len = rankings.length; i < len; i++) {
		    if(rankings[i].token == token){
		    	$scope.myRank = rankings[i];
		    	break;
		    };
		}
		//$scope.myRank = rankings.find(x => x.token === token);
		console.log($scope.myRank);
	},
	$scope.goTo = function(url){
		$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
		$location.path(url);
	}

	$scope.getMyRank($scope.stats.rankings, $scope.currentUser.token);

	if(!$scope.currentUser.picture){
		$scope.currentUser.picture = "img/avatar-"+$scope.currentUser.gender+".png";
	}

	$scope.$on('$ionicView.enter', function(e) {
		if($state.current.name == 'app.quiz-no_correct' && $scope.activeQuiz.quiz_no_winner_image){
			$scope.openModal();
			setTimeout(function(){
				$scope.closeModal();
			}, 10000);
		}
	});


	$ionicModal.fromTemplateUrl('templates/quiz/_noWinnersModal.html', {
		scope: $scope

	}).then(function(modal) {
		$scope.modal = modal;
	});
	$scope.openModal = function() {
		$scope.modal.show();
	};
	$scope.closeModal = function() {
		$scope.modal.hide();
	};
	// Cleanup the modal when we're done with it!
	$scope.$on('$destroy', function() {
		$scope.modal.remove();
	});

})

.controller('MotmCtrl', function($ionicScrollDelegate, Flash, $location, $ionicHistory, $scope, $rootScope, myStorage, $q, ApiEndpoint, FeedService, $ionicLoading, $ionicPopup) {
	
	$scope.playerSelection = null;
	$scope.isLoadingContent = false;
	$rootScope.lastSubmittedMotmId = myStorage.getObj('lastSubmittedMotmId');
	$rootScope.lastPlayerSelection = myStorage.getObj('lastPlayerSelection');

	$scope.$on('$ionicView.enter', function(e) {
		if(!$rootScope.motm || !$rootScope.lastSubmittedMotmId || ($rootScope.lastSubmittedMotmId != myStorage.getObj('lastSubmittedMotmId'))){
			$scope.doRefresh(true);
		}
	});

	$scope.doRefresh = function(loading){
		return $q(function(resolve, reject){
			if(loading){
				$scope.isLoadingContent = true;
			}
			var url = ApiEndpoint.url+"/motms";
			FeedService.get(url)
			.then(function successCallback(res){
				$rootScope.motm = res.data.motm;

				/* hide loading spinner */
				$scope.isLoadingContent = false;

				$ionicLoading.hide();
				$scope.$broadcast('scroll.refreshComplete');
				resolve();

			}, function errorCallback(res){
				console.log("Error parsing or getting data from api");
				$scope.$broadcast('scroll.refreshComplete');
				$ionicLoading.hide();
				$ionicPopup.alert({
					title: 'Der opstod en fejl!',
					content: 'Der kunne ikke hentes data'
				});

				reject();

			})
		})
	},
	$scope.setVote = function(player_id, player){
		$scope.playerSelection = player_id;
		$rootScope.lastPlayerSelection = player;
		myStorage.setObj('lastPlayerSelection', player);
	}
	$scope.submitVote = function(motm_id){
		$ionicLoading.show();
		var currentUser = myStorage.getObj('currentUser');

		FeedService.post(ApiEndpoint.url+"/motms/vote/register", {"token" : currentUser.token, "motm_id" : motm_id, "player_id": $scope.playerSelection})
		.then(function successCallback(res){
			$ionicLoading.hide();

			if(res.data.result == 'success'){

				$rootScope.lastSubmittedMotmId = motm_id;
				myStorage.setObj('lastSubmittedMotmId', motm_id);

				$scope.playerSelection = null;

				Flash.clear();
				$ionicScrollDelegate.scrollTop();
			}
			else{
				if(res.data.already_registered){

					myStorage.setObj('lastSubmittedMotmId', motm_id);
					$scope.lastSubmittedMotmId = motm_id;

					$scope.playerSelection = null;
					$ionicScrollDelegate.scrollTop();
				}
				$ionicPopup.alert({
					title: 'Der opstod en fejl!',
					content: res.data.msg
				});		
			}
		})
	}
})

.controller('FanzoneCtrl', function(ApiEndpoint, FeedService, $location, $cordovaFacebook, $ionicHistory, UserService, $scope, $rootScope, myStorage) {
		$scope.$on('$ionicView.enter', function(e) {
			if(!$rootScope.fanzoneimage){
				$scope.getFanzoneImage();
			}
			if(!$rootScope.user){
				$scope.checkUserForChanges();
			}
		});

		$scope.checkUserForChanges = function(){

			UserService.checkUserLogin()
			.then(function succesCallback(){//if current user has active login

				UserService.checkUserForChanges()
				.then(function successCallback(user){
						$rootScope.user = user;
						/* Set placeholder if user has no picture */
						if(!$rootScope.user.picture){
							$rootScope.user.picture = "img/avatar-"+$rootScope.user.gender+".png";
						}
					}, 
					function errorCallback(user){
						if(user && !$rootScope.user){ //dont update user if nothing has changed except if no user is set
							$rootScope.user = user;
							/* Set placeholder if user has no picture */
							if(!$rootScope.user.picture){
								$rootScope.user.picture = "img/avatar-"+$rootScope.user.gender+".png";
							}
						}
				});


			}, 
			function errorCallback(){ //if current userlogin is invalid
				UserService.logout();
			});
			
		},
		$scope.logout = function(){
			UserService.logout();
		},
		$scope.getFanzoneImage = function(){
			var url = ApiEndpoint.url+"/app/image/fanzone";
			FeedService.get(url)
			.then(function successCallback(res){

				if(res.data.image){
					$rootScope.fanzoneimage = res.data.image;
				}

			}, function errorCallback(res){
				console.log("Error parsing or getting data from api");
			})
		}


})

.controller('SpectatorsCtrl', function($ionicScrollDelegate, $http, Flash, $location, $ionicHistory, $scope, $rootScope, myStorage, $q, ApiEndpoint, FeedService, $ionicLoading, $ionicPopup) {
	
	$scope.isLoadingContent = false;
	$rootScope.lastSubmittedSpectatorsId = myStorage.getObj('lastSubmittedSpectatorsId');
	$rootScope.lastSubmittedSpectatorsGuess = myStorage.getObj('lastSubmittedSpectatorsGuess');
	$scope.guess = {'number' : ''};

	$scope.$on('$ionicView.enter', function(e) {

		if(!$rootScope.spectators || !$rootScope.lastSubmittedSpectatorsId || ($rootScope.lastSubmittedSpectatorsId != myStorage.getObj('lastSubmittedSpectatorsId'))){
			$scope.doRefresh(true);
		}
	});

	$scope.doRefresh = function(loading){
		return $q(function(resolve, reject){
			if(loading){
				$scope.isLoadingContent = true;
			}
			var url = ApiEndpoint.url+"/spectators";
			FeedService.get(url)
			.then(function successCallback(res){
				$rootScope.spectators = res.data.spectator;

				/* hide loading spinner */
				$scope.isLoadingContent = false;

				$ionicLoading.hide();
				$scope.$broadcast('scroll.refreshComplete');
				resolve();

			}, function errorCallback(res){
				console.log("Error parsing or getting data from api");
				$scope.$broadcast('scroll.refreshComplete');
				$ionicLoading.hide();
				$ionicPopup.alert({
					title: 'Der opstod en fejl!',
					content: 'Der kunne ikke hentes data'
				});

				reject();

			})
		})
	},
	$scope.checkGuess = function(guess){
		$scope.guess.number = $scope.guess.number.replace(/\D/g,'');
	},
	
	$scope.submitGuess = function(spectator_id, guess){
		if(guess > 0){
			$ionicLoading.show();

			// $ionicLoading.show();
			var currentUser = myStorage.getObj('currentUser');

			FeedService.post(ApiEndpoint.url+"/spectators/guess/register", {"token" : currentUser.token, "spectator_id" : spectator_id, "guess": guess})
			.then(function successCallback(res){
				$ionicLoading.hide();
				$scope.$broadcast('scroll.refreshComplete');


				if(res.data.result == 'success'){

					$rootScope.lastSubmittedSpectatorsId = spectator_id;
					myStorage.setObj('lastSubmittedSpectatorsId', spectator_id);

					$scope.lastSubmittedSpectatorsGuess = guess;
					myStorage.setObj('lastSubmittedSpectatorsGuess', guess);

					Flash.clear();
					$ionicScrollDelegate.scrollTop();
				}
				else{
					if(res.data.already_registered){

						myStorage.setObj('lastSubmittedSpectatorsId', spectator_id);
						$scope.lastSubmittedSpectatorsId = spectator_id;

						$scope.lastSubmittedSpectatorsGuess = null;
						$ionicScrollDelegate.scrollTop();
					}
					$ionicPopup.alert({
						title: 'Der opstod en fejl!',
						content: res.data.msg ? res.data.msg : "Der opstod et problem i din forbindelse til serveren"
					});	
				}	

			})
		}
	}
})

.controller('SurveyCtrl', function($timeout, $stateParams, $state, $ionicScrollDelegate, $http, Flash, $location, $ionicHistory, $scope, $rootScope, myStorage, $q, ApiEndpoint, FeedService, $ionicLoading, $ionicPopup) {
	
	$rootScope.canClick = true;
	$scope.isLoadingContent = false;
	$rootScope.submittedSurveys = myStorage.getObj('submittedSurveys');
	if(!$rootScope.submittedSurveys){
		$rootScope.submittedSurveys = [];
	}
	// $scope.guess = {'number' : ''};
	$scope.animate = false;
	$scope.animation = {direction: 'slide'};
	$scope.currentUser = myStorage.getObj('currentUser');

	$scope.$on('$ionicView.enter', function(e) {

		$scope.animate = true;

		if(!$rootScope.surveys){
			$scope.doRefresh(true);
		}
	});

	$scope.doRefresh = function(loading){
		return $q(function(resolve, reject){
			if(loading){
				$scope.isLoadingContent = true;
			}
			var url = ApiEndpoint.url+"/surveys";
			FeedService.get(url)
			.then(function successCallback(res){
				$rootScope.surveys = res.data.surveys;

				/* hide loading spinner */
				$scope.isLoadingContent = false;

				$ionicLoading.hide();
				$scope.$broadcast('scroll.refreshComplete');
				resolve();

			}, function errorCallback(res){
				console.log("Error parsing or getting data from api");
				$scope.$broadcast('scroll.refreshComplete');
				$ionicLoading.hide();
				$ionicPopup.alert({
					title: 'Der opstod en fejl!',
					content: 'Der kunne ikke hentes data'
				});

				reject();

			})
		})
	},
	$scope.checkIfSubmitted = function(survey_id){
		return $rootScope.submittedSurveys.indexOf(survey_id) == -1 ? false : true;
	},
	$scope.goToSurvey = function(survey){
		$rootScope.activeSurvey = survey;
		$rootScope.activeSurveyAnswers = [];
		$rootScope.activeSurveyIndex = 0;
		$scope.animation.direction = 'slide';

		// $ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
		$state.go('app.survey.question');
	},
	$scope.submitSurveyAnswer = function(answer, question_id){
		if($rootScope.canClick){
			$rootScope.canClick = false;		
			$rootScope.activeSurveyAnswers[$rootScope.activeSurveyIndex] = {question_id: question_id, answer_id: answer.id};
			$scope.animation.direction = 'slide';

			$timeout(function(){
				$rootScope.canClick = true;
				$rootScope.activeSurveyIndex++;
				
				if($rootScope.activeSurveyIndex >= $rootScope.activeSurvey.questions.length){
					
					$state.go('app.survey.complete');
				}
				else{		

					$scope.transitionToQuestion();
				}
			}, 300);
		}

	},
	$scope.goBack = function(){
		if($rootScope.canClick){
			$rootScope.canClick = false;		

			$scope.animation.direction = 'slide-back';
			$timeout(function(){
				$rootScope.canClick = true;		
				$rootScope.activeSurveyIndex--;
				$scope.transitionToQuestion();
			}, 300);
		}

	},
	$scope.transitionToQuestion = function(){
		var newState = 'app.survey.question';
		if($state.current.name == 'app.survey.question'){
			newState = 'app.survey.question2';
		}
		$state.go(newState);
	},
	$scope.submitSurvey = function(){

		$ionicLoading.show();

		var data = {'token' : $scope.currentUser.token, survey_id: $scope.activeSurvey.id, answers: $scope.activeSurveyAnswers};
		FeedService.post(ApiEndpoint.url+"/surveys/submit", data)
		.then(function successCallback(res){
			$ionicLoading.hide();


			if(res.data.result == 'success'){
				$rootScope.submittedSurveys.push($scope.activeSurvey.id);
				myStorage.setObj('submittedSurveys', $rootScope.submittedSurveys);

				$rootScope.activeSurvey = null;
				$rootScope.activeSurveyIndex = null;
				$rootScope.activeSurveyAnswers = null;

				$ionicHistory.goBack(-1);

		// 		Flash.clear();
		// 		$ionicScrollDelegate.scrollTop();
			}
			else{
				if(res.data.already_registered){

					$rootScope.submittedSurveys.push($scope.activeSurvey.id);
					myStorage.setObj('submittedSurveys', $rootScope.submittedSurveys);
	
					$ionicHistory.goBack(-1);


		// 			$scope.playerSelection = null;
		// 			$ionicScrollDelegate.scrollTop();
				}
				$ionicPopup.alert({
					title: 'Der opstod en fejl!',
					content: res.data.msg
				});		
			}
		})
	
	}
})

.controller('PlayersCtrl', function(ApiEndpoint, $rootScope, $scope, $state, myStorage, FeedService, $ionicLoading, $ionicModal, $ionicPopup) {
	
	$scope.isLoadingContent = false;
	
	$scope.$on('$ionicView.enter', function(e) {

		if(!$rootScope.players){
				$scope.doRefresh(true);
		}
	});

	$scope.doRefresh = function(loading){
		if(loading){
			$scope.isLoadingContent = true;
		}
		var url = ApiEndpoint.url+"/app/players";
		FeedService.get(url)
		.then(function successCallback(res){
			console.log(res.data);
			$rootScope.players = res.data.players;

			/*hide loading spinner*/
			$scope.isLoadingContent = false;
			$ionicLoading.hide();
			$scope.$broadcast('scroll.refreshComplete');

		}, function errorCallback(res){
			console.log("Error parsing or getting data from randersfc");
			/* Stop the ion-refresher from spinning */
			$ionicLoading.hide();
			$scope.isLoadingContent = false;
			$scope.$broadcast('scroll.refreshComplete');
			$ionicPopup.alert({
				title: 'Der opstod en fejl!',
				content: 'Der kunne ikke hentes data fra RandersFC.dk'
			});
		})
	}


	/* show loading spinner when the page loads */
	//$ionicLoading.show();
	/* do initial pagerefresh when page loads. */

})

.controller('PlayerCtrl', function($scope, $state, $rootScope) {
	$scope.player = $rootScope.players[$state.params.playerIndex];
})