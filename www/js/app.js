// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('randersfc', ['ui.carousel','ionic', 'ngCordova', 'randersfc.controllers', 'randersfc.services', 'ngIOS9UIWebViewPatch', 'randersfc.customFilters', 'angular-bind-html-compile', 'timer', 'ngFlash', 'btford.socket-io', 'angularMoment'])

.constant("ApiEndpoint", {
  // "url": "http://www.side-walk.dk/api/rfc",
  "url": "http://quiz.shosting.dk/api/rfc",
  // "url": "http://rfc.local/api/rfc",

  // "new_url":   "http://www.side-walk.dk/api/rfc",

  //"rfc_url":   "http://rfc.webcamp.dk/app"
  "rfc_url":   "http://www.randersfc.dk/app"
})

.constant('angularMomentConfig', {
    timezone: 'Europe/Copenhagen' // e.g. 'Europe/London'
})

.run(function(Flash, $cordovaNetwork, PushService, ApiEndpoint, $ionicPlatform, $http, myStorage, $rootScope) {
    moment.locale('da');

	$ionicPlatform.ready(function() {
	// //If user uses the power button to sleep
			// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard for form inputs)
			if (window.cordova){

				//check if keyboard is available
				if(window.cordova.plugins.Keyboard) {
					cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
					cordova.plugins.Keyboard.disableScroll(true);
				}

				//check if statusbar is available
				if (window.StatusBar) {
					// org.apache.cordova.statusbar required
					StatusBar.styleLightContent();
					//StatusBar.styleDefault();
				}

				//check network status
				$rootScope.networkState = $cordovaNetwork.isOnline();
				//listen for Online event 
				$rootScope.$on('$cordovaNetwork:online', function(event, networkState){
					$rootScope.networkState = true;
					console.log("net online: "+networkState);
				})

				// listen for Offline event
				$rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
					$rootScope.networkState = false;
					console.log("net offline: "+networkState);
				})
				

				//define os
			   	$rootScope.deviceOS = "not defined";
				if(ionic.Platform.isIOS() || ionic.Platform.isIPad()){
					$rootScope.deviceOS = 'ios';
				}
				else{
					$rootScope.deviceOS = 'android';	
				}

				// // PUSH-MESSAGES
				// var pushOptions = {
				// 	android: {
				// 		icon: "icon.png",
				// 		iconColor: "#0f3f6c",
				// 		senderID: "332491274203"
				// 	},
				// 	ios: {
				// 		alert: "true",
				// 		badge: "true",
				// 		sound: "true",
				// 		clearBadge: "true",
				// 	},
				// 	windows: {}
				// };

				//Check if FCMplugin is available
				if (typeof FCMPlugin != 'undefined') {

					//Register token on load
					FCMPlugin.getToken(function (token) {
						if(token){

					    	console.log('updating push-token: '+token);
					    	
					    	//Set token data
					    	var data = {};
					    	data.regid = token;
					    	data.device = $rootScope.deviceOS;

					    	var currentUser = myStorage.getObj('currentUser');
					    	if(currentUser){
					    		data.user_id = currentUser.user_id;
					    	}

					    	PushService.registerToken(data);
						}

					});

					//If new token is aquired update it
					FCMPlugin.onTokenRefresh(function (token) {
						console.log('updating push-token: '+token);

				    	//Set token data
				    	var data = {};
				    	data.regid = token;
				    	data.device = $rootScope.deviceOS;


				    	var currentUser = myStorage.getObj('currentUser');
				    	if(currentUser){
				    		data.user_id = currentUser.user_id;
				    	}

				    	PushService.registerToken(data);

				    });
					
					//If message was recieved
				FCMPlugin.onNotification(function(data){
					console.log('push-message received');
						if(data.wasTapped){
					      //Notification was received on device tray and tapped by the user.
					    }else{
					      //Notification was received in foreground. Maybe the user needs to be notified.
					      // alert( JSON.stringify(data) );
					      if(data.message){


					      	PushService.show('Randers FC', data.message);
					      	// $rootScope.pushMessage = null;
					      	// $rootScope.pushMessage = data.message;

					      	// setTimeout(function() {
					      	// 	$rootScope.pushMessage = null;
					      	// }, 10000);

					      }
					    }
					});

				}
				else{
						console.log('FCMPlugin not available');
				}



				// initialize push
				// $cordovaPushV5.initialize(pushOptions).then(function() {


				//     // start listening for new notifications
				//     $cordovaPushV5.onNotification();

				//     // start listening for errors
				//     $cordovaPushV5.onError();
				    
				//     // register to get registrationId
				//     $cordovaPushV5.register().then(function(registrationId) {
				//     	// console.log('updating push-token: '+registrationId);

				//     	// var data = {};
				//     	// data.regid = registrationId;
				//     	// data.device = $rootScope.deviceOS;

				//     	// var currentUser = myStorage.getObj('currentUser');
				//     	// if(currentUser){
				//     	// 	data.user_id = currentUser.user_id;
				//     	// }

				//     	// $http.post(ApiEndpoint.url +'/app/notifications/register', data)
				//     	// .success(function(response){
				//     	// 	console.log('id sent - response: '+response);
				//     	// 	myStorage.set('regid', registrationId);
				//     	// })
				//     	// .error(function(response){
				//     	// 	console.log("error in push registration");
				//     	// 	console.log(response);
				//     	// })
				//     }, function(){
				//     	console.log("error registering push");
				//     })
				// });
	  	
	  			  // triggered every time notification received
	  	// 		$rootScope.$on('$cordovaPushV5:notificationReceived', function(event, data){

	  	// 			$rootScope.pushMessage = data.message;

	  	// 			setTimeout(function() {
	  	// 				$rootScope.pushMessage = null;
	  	// 			}, 10000);

	  	// 			console.log('PUSH-message recieved '+data.message);
				//     // data.message,
				//     // data.title,
				//     // data.count,
				//     // data.sound,
				//     // data.image,
				//     // data.additionalData
				// });

				// // triggered every time error occurs
				// $rootScope.$on('$cordovaPushV5:errorOcurred', function(event, e){
	   //  			console.log("error in push message");
	   //  			console.log(e.message);// e.message
				// });
			}
			else{
				// myStorage.set('regid', '538685def5b2539bee3a7247d8b7adecc22f834752cdc7c5c40f68a75b853091');
				$rootScope.networkState = true;
			}
					// setTimeout(function() {
					//       	PushService.show('Randers FC', 'test');
	  		// 		}, 2000);
	  		// 							setTimeout(function() {
					//       	PushService.show('Randers FC', 'test2');
	  		// 		}, 4000);


	});

})


.config(function($ionicConfigProvider, $cordovaInAppBrowserProvider,  $httpProvider){

	//Set content of back-button
	$ionicConfigProvider.backButton.text('');

    //initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};    
    }    

    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
	
})


.config(function($stateProvider, $urlRouterProvider) {
	$stateProvider

	.state('app', {
		url: '/app',
		abstract: true,
		templateUrl: 'templates/menu.html',
		controller: 'LeftMenuCtrl'
	})

	.state('app.home', {
		url: '/home',
		views: {
			'menuContent': {
				templateUrl: 'templates/home.html',
				controller: 'HomeCtrl'
			}
		}
	})

	.state('app.tv', {
		url: '/tv',
		views: {
			'menuContent': {
				templateUrl: 'templates/tv.html',
				controller: 'TvCtrl'
			}
		}
	})

	.state('app.video', {
		url: '/tv/:videoIndex',
		views: {
			'menuContent': {
				templateUrl: 'templates/video.html',
				controller: 'VideoCtrl'
			}
		}
	})

	.state('app.matches', {
		url: '/matches',
		views: {
			'menuContent': {
				templateUrl: 'templates/matches.html',
				controller: 'MatchesCtrl'

			}
		}
	})

	.state('app.results', {
		url: '/results',
		views: {
			'menuContent': {
				templateUrl: 'templates/results.html',
				controller: 'ResultsCtrl'
			}
		}
	})

	.state('app.newslist', {
		url: '/news',
		views: {
			'menuContent': {
				templateUrl: 'templates/newslist.html',
				controller: 'NewslistCtrl'
			}
		}
	})

	.state('app.news', {
		url: '/news/:newsIndex',
		views: {
			'menuContent': {
				templateUrl: 'templates/news.html',
				controller: 'NewsCtrl'
			}
		}
	})

	.state('app.positions', {
		url: '/positions',
		views: {
			'menuContent': {
				templateUrl: 'templates/positions.html',
				controller: 'PositionsCtrl'
			}
		}
	})

	.state('app.lineup', {
		url: '/lineup',
		views: {
			'menuContent': {
				templateUrl: 'templates/lineup.html',
			}
		}
	})	


	.state('app.players', {
		url: '/players',
		views: {
			'menuContent': {
				templateUrl: 'templates/players.html',
				controller: 'PlayersCtrl'
			}
		}
	})

	.state('app.player', {
		url: '/players/:playerIndex',
		views: {
			'menuContent': {
				templateUrl: 'templates/player.html',
				controller: 'PlayerCtrl'
			}
		}
	})	

	.state('app.settings', {
		url: '/settings',
		views: {
			'menuContent': {
				templateUrl: 'templates/settings.html',
				controller: 'SettingsCtrl'
			}
		}
	})

	// FANZONE
	.state('app.fanzone', {
		url: '/fanzone/overview',
		views: {
			'menuContent': {
				templateUrl: 'templates/fanzone.html',
				controller: 'FanzoneCtrl'
			}
		},
		onEnter: function(myStorage, $state, $ionicHistory){
			if(!myStorage.getObj('currentUser')){
				$state.transition.finally(function(){
					$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true})
					$state.go('app.user-login');
				});
			}		
		}		
	})

	.state('app.motm', {
		url: '/fanzone/motm',
		views: {
			'menuContent': {
				templateUrl: 'templates/motm.html',
				controller: 'MotmCtrl'
			}
		},
		onEnter: function(myStorage, $state, $ionicHistory){
			if(!myStorage.getObj('currentUser')){
				$state.transition.finally(function(){
					$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true})
					$state.go('app.user-login');
				});
			}		
		}
	})

	.state('app.spectators', {
		url: '/fanzone/spectators',
		views: {
			'menuContent': {
				templateUrl: 'templates/spectators.html',
				controller: 'SpectatorsCtrl'
			}
		},
		onEnter: function(myStorage, $state, $ionicHistory){
			if(!myStorage.getObj('currentUser')){
				$state.transition.finally(function(){
					$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true})
					$state.go('app.user-login');
				});
			}		
		}
	})



	.state('app.surveys', {
		url: '/fanzone/surveys',
		views: {
			'menuContent': {
				templateUrl: 'templates/survey/surveys.html',
				controller: 'SurveyCtrl'
			}
		},
		onEnter: function(myStorage, $state, $ionicHistory){
			if(!myStorage.getObj('currentUser')){
				$state.transition.finally(function(){
					$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true})
					$state.go('app.user-login');
				});
			}		
		}
	})
	.state('app.survey', {
		url: '/fanzone/survey/start',
		views: {
			'menuContent' : {
				templateUrl: 'templates/survey/survey.html',
				controller: 'SurveyCtrl'
			}
		}
	})
	.state('app.survey.question', {
		url: '/fanzone/survey/question',
		templateUrl: 'templates/survey/survey-question.html',
	})
	.state('app.survey.question2', {
		url: '/fanzone/survey/question',
		templateUrl: 'templates/survey/survey-question.html',
	})
	.state('app.survey.complete', {
		url: '/fanzone/survey/complete',
		templateUrl: 'templates/survey/survey-complete.html',
	})


	.state('app.prize-list', {
		url: '/fanzone/prize-list',
		views: {
			'menuContent': {
				templateUrl: 'templates/prizes/prize-list.html',
				controller: 'PrizeListCtrl'
			}
		},
		onEnter: function(myStorage, $state, $ionicHistory){
			if(!myStorage.getObj('currentUser')){
				$state.transition.finally(function(){
					$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true})
					$state.go('app.user-login');
				});
			}		
		}
	})

	.state('app.prize', {
		url: '/fanzone/prize/:prizeIndex',
		views: {
			'menuContent': {
				templateUrl: 'templates/prizes/prize.html',
				controller: 'ShowPrizeCtrl'
			}
		},
		onEnter: function(myStorage, $state, $ionicHistory){
			if(!myStorage.getObj('currentUser')){
				$state.transition.finally(function(){
					$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true})
					$state.go('app.user-login');
				});
			}		
		}
	})

	.state('app.quiz-start', {
		url: '/fanzone/quiz-start',
		views: {
			'menuContent': {
				templateUrl: 'templates/quiz/quiz-start.html',
				controller: 'QuizStartCtrl'
			}
		},
		onEnter: function(myStorage, $state, $ionicHistory){
			if(!myStorage.getObj('currentUser')){
				$state.transition.finally(function(){
					$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true})
					$state.go('app.user-login');
				});
			}		
		}
	})



	.state('app.user-create', {
		url: '/fanzone/user-create',
		views: {
			'menuContent': {
				templateUrl: 'templates/user/user-create.html',
				controller: 'CreateUserCtrl'
			}
		}
	})

	.state('app.user-do-login', {
		url: '/fanzone/user-do-login',
		views: {
			'menuContent': {
				templateUrl: 'templates/user/user-do-login.html',
				controller: 'UserLoginCtrl'
			}
		}
	})

	.state('app.user-login', {
		url: '/fanzone/user-login',
		views: {
			'menuContent': {
				templateUrl: 'templates/user/user-login.html',
				controller: 'UserLoginCtrl'
			}
		}
	})
	
	.state('app.forgot-password', {
		url: '/fanzone/forgot-password',
		views: {
			'menuContent': {
				templateUrl: 'templates/user/forgot-password.html',
				controller: 'ForgotPasswordCtrl'
			}
		}
	})
	.state('app.reset-password', {
		url: '/fanzone/reset-password',
		views: {
			'menuContent': {
				templateUrl: 'templates/user/reset-password.html',
				controller: 'ForgotPasswordCtrl'
			}
		}
	})
	.state('app.pick-new-password', {
		url: '/fanzone/pick-new-password',
		views: {
			'menuContent': {
				templateUrl: 'templates/user/pick-new-password.html',
				controller: 'ForgotPasswordCtrl'
			}
		}
	})

	.state('app.myprofile', {
		url: '/fanzone/myprofile',
		views: {
			'menuContent': {
				templateUrl: 'templates/user/my-profile.html',
				controller: 'MyProfileCtrl'
			}
		}
	})


// .state('app.motm-user-create', {
// 		url: '/fanzone/motm/user-create',
// 		views: {
// 			'menuContent': {
// 				templateUrl: 'templates/user/user-create.html',
// 				controller: 'CreateUserCtrl'
// 			}
// 		}
// 	})

// 	.state('app.motm-user-do-login', {
// 		url: '/fanzone/motm/user-do-login',
// 		views: {
// 			'menuContent': {
// 				templateUrl: 'templates/user/user-do-login.html',
// 				controller: 'UserLoginCtrl'
// 			}
// 		}
// 	})

// 	.state('app.motm-user-login', {
// 		url: '/fanzone/motm/user-login',
// 		views: {
// 			'menuContent': {
// 				templateUrl: 'templates/user/user-login.html',
// 				controller: 'UserLoginCtrl'
// 			}
// 		}
// 	})
	

// 	.state('app.motm-user-show', {
// 		url: '/fanzone/motm/user-show',
// 		views: {
// 			'menuContent': {
// 				templateUrl: 'templates/user/user-show.html',
// 				controller: 'UserShowCtrl'
// 			}
// 		}
// 	})


	.state('app.quizroom', {
		url: '/fanzone/quiz/quizroom',
		views: {
			'menuContent' : {
				templateUrl: 'templates/quiz/quizroom.html',
				controller: 'QuizCtrl'
			}
		}
	})
	.state('app.quizroom.countdown', {
		url: '/fanzone/quiz/countdown',
		templateUrl: 'templates/quiz/quizroom-countdown.html',
	})
	.state('app.quizroom.question', {
		url: '/fanzone/quiz/question',
		templateUrl: 'templates/quiz/quizroom-question.html',
	})
	.state('app.quizroom.int_question', {
		url: '/fanzone/quiz/int_question',
		templateUrl: 'templates/quiz/quizroom-int_question.html',
	})
	// .state('app.quizroom.working', {
	// 	url: '/working',
	// 	templateUrl: 'templates/quiz/quizroom-working.html',
	// })
	.state('app.quizroom.stats', {
		url: '/fanzone/quiz/stats',
		templateUrl: 'templates/quiz/quizroom-stats.html',
	})
	.state('app.quizroom.final_stats', {
		url: '/fanzone/quiz/final_stats',
		templateUrl: 'templates/quiz/quizroom-final_stats.html',
	})

	.state('app.quiz-final_rank', {
		url: '/fanzone/quiz/final_rank',
		views: {
			'menuContent': {
				templateUrl: 'templates/quiz/quiz-final_rank.html',
				controller: 'QuizResultCtrl'
			}
		}
	})

	// .state('app.quiz-loser', {
	// 	url: '/quiz/loser',
	// 	views: {
	// 		'menuContent': {
	// 			templateUrl: 'templates/quiz/quiz-loser.html',
	// 			controller: 'QuizResultCtrl'
	// 		}
	// 	}
	// })

	.state('app.quiz-result', {
		url: '/fanzone/quiz/result',
		views: {
			'menuContent': {
				templateUrl: 'templates/quiz/quiz-result.html',
				controller: 'QuizResultCtrl'
			}
		}
	})

	.state('app.quiz-no_correct', {
		url: '/fanzone/quiz/no_correct',
		views: {
			'menuContent': {
				templateUrl: 'templates/quiz/quiz-result.html',
				controller: 'QuizResultCtrl'
			}
		}
	})

	.state('app.quiz-error', {
		url: '/fanzone/quiz/error',
		views: {
			'menuContent': {
				templateUrl: 'templates/quiz/quiz-error.html',
			}
		}
	});
	// .state('app.quiz-loser', {
	// 	url: 'quiz/loser',
	// 	templateUrl: 'templates/quiz/quizroom-loser.html',
	// })



	$urlRouterProvider
	.when('/app/fanzone', ['$rootScope', '$state','myStorage', function ($rootScope, $state, myStorage) {
        if(!myStorage.getObj('currentUser')){
			$state.go('app.user-login');
		}
		else{
			$state.go('app.fanzone');	
		}
	}])
	.when('/app/fanzone/quiz', ['$rootScope', '$state','myStorage', function ($rootScope, $state, myStorage) {
			if(!myStorage.getObj('activeQuiz')){
				$state.go('app.quiz-start');
			}
			else{

				if(new Date(myStorage.getObj('activeQuiz').quiz_starttime) > new Date()){
					$state.go('app.quizroom.countdown');
				}
				else{
					myStorage.remove('activeQuiz');
					$state.go('app.quiz-start');

				}
			}
	}])



	// if none of the above states are matched, use this as the fallback
	.otherwise('/app/home');

})
