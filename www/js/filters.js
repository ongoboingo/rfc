angular.module('randersfc.customFilters', ['ngSanitize'])

.filter('convertDate', function() {
  return function(input) {
    input = input.replace('+0100', '');
    return new Date(input);
  };
})

.filter('is_today', function() {
  return function(input) {
    // moment.locale('da');

    var input_day = moment(input, 'DD/MM/YYYY').startOf('day');
    var today = moment().startOf('day');
    if(today.diff(input_day, 'days') == 0){
      return true;
    }
    else{
      return false;
    }
  };
})

.filter('is_before_now', function() {
  return function(input) {

    var input_time = moment(input,'H:mm').unix();
    var now = moment().unix();

    if(now > input_time){
      return true;
    }
    return false
  };
})

.filter('is_after_now', function() {
  return function(input) {
    var input_time = moment(input,'H:mm').unix();
    var now = moment().unix();

    if(now < input_time){
      return true;
    }
    return false
  };
})

.filter('calibrateTimer', function() {
  return function(input, timeDiff) {
    var newVal = Number(input) + Number(timeDiff);//timeDiff;
    return Math.round(newVal);
  };
})

.filter('base64_decode', function($sce) {
  return function(input) {

    return $sce.trustAsHtml(atob(input));
  };
})

.filter('nl2br', function() {
  return function(input) {
    return input.replace(/(?:\r\n|\r|\n)/g, '<br />');
  };
})

.filter('toTrusted', function($sce) {
  return function(input) {
    return  $sce.trustAsHtml(input);
  };
})

.filter('trustAsResourceUrl', ['$sce', function($sce) {
    return function(val) {
        return $sce.trustAsResourceUrl(val);
    };
}])

.filter('cleanHtml', function() {
  return function(input) {

    //Remove line breaks


    input = input.replace(new RegExp('\r', 'g'), '');
    input = input.replace(new RegExp('\n', 'g'), '');

    //Remove html-tags - with exceptions
    input = input ? String(input).replace(/<(?!img|em|\/em|a|\/a|strong|\/strong|p|\/p|br|iframe|\/iframe).*?\>/gm, '') : '';

    //handle external links links
    input = input.replace(new RegExp('<a.*?href=', 'g'), '<a external="true" href=');

    //Remove style element from tags
    input = input ? String(input).replace(/style=\".*?\"/gm, '') : '';
    input = input ? String(input).replace(/width=\".*?\"/gm, '') : '';
    input = input ? String(input).replace(/height=\".*?\"/gm, '') : '';

    //Check for iframes and insert it into a container-div
    input = input.replace(/<iframe.*?src=".*?youtube.*?\/embed\//gm, '<div class="video-container"><youtube videoid="');
    input = input.replace(/<\/iframe>/gm, '</youtube></div> ');

   /* input = input.replace(/<iframe /gm, '<div class="video-container"><iframe data-tap-disabled="true" youtube="true" ');
    input = input.replace(/<\/iframe>/gm, '</iframe></div> ');*/
    
    return input;
  };
})

.filter('checkForYoutubeVideos', function() {
  return function(input) {
    if(input.match(/<iframe.*?src=".*?youtube.*?\/embed\//gm)){
      return true;
    }   
    else{
      return false;
    }
  };
})


.filter('removeHtml', function() {
  return function(input) {

    input = String(input).replace(/<[^>]+>/gm, '');

    return input;
  };
})

.filter('removeHtmlAndCut', function() {
  return function(input, max) {
    input = String(input).replace(/<[^>]+>/gm, '');
    
    var buffer = 7;

    if(input.length > max){
      input = input.substring(0, max);

      var lastChars = input.substring(input.length-buffer, input.length);
      var pattern = new RegExp("&");
      if(position = pattern.exec(lastChars)){
        input = input.substring(0, input.length-(buffer-position.index));
      }
    }
    // if(lastChars.test('&')){
    //   input = "test";
    // }

    return input;
  };
})