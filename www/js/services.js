angular.module('randersfc.services', [])

.directive('player', function() {
	return {
		//template: 'Name: {{customer.name}} Address: {{customer.address}}'
		restrict: 'E',
		link: function(scope, elem, attrs){
			
			elem.replaceWith("<div class=\"video-container\"><iframe src=\"https://www.youtube.com/embed/"+attrs.src+"?rel=0\" frameborder=\"0\" width=\"560\" height=\"315\"></iframe></div>");
		}
	};
})


.directive('external', function($cordovaInAppBrowser) {

	var options = {
		location: 'no',
		toolbar: 'yes',
		enableViewportScale: 'yes',
		toolbarposition: 'top',
		closebuttoncaption: 'Luk'
	};

	return {
		//template: 'Name: {{customer.name}} Address: {{customer.address}}'
		restrict: 'A',
		link: function(scope, elem, attrs){
			//elem.replaceWith('<a href="#" link="\''+attrs.href+'\'">'+elem.html()+'</a>');
			elem.bind('click', function(e){
				e.preventDefault();
				if(attrs.href != 'null' && attrs.href != ''){
					$cordovaInAppBrowser.open(attrs.href, '_blank', options);
				}
			})
		}
	};
})

.directive('appVersion', function($cordovaAppVersion) {
	return function(scope, elm, attrs) {
		if(window.cordova){		
			console.log('cordova found')
			$cordovaAppVersion.getVersionNumber().then(function (version) {
				console.log(version);
				elm.text(version);
			}, false);			
		}
		else{
			elm.text('ikke tilgængelig');

		}
	};
})

.factory('FeedService', function($ionicLoading, $http, $q, myStorage, $ionicHistory, $location, $ionicPopup){
	var youtubeAPIKey = 'AIzaSyC0iR_5gtXEkZzRhffmMwznw6-5wkMEPiE';
	return {
		get: function(url){
			return $http.get(url);
		},
		post: function(url, data, supressErrors){
			return $q(function(resolve, reject){
				$http.post(url, data)
				.then(
					function successCallback(res){
						console.log(res);
						//check if token was declined and user should be logged out
						if(res.data.token_accepted === false){
							myStorage.remove('currentUser');
							$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
							$ionicLoading.hide();
							$location.path('/app/fanzone/user-login');
							reject();
						}
						else{
							resolve(res);
						}
					}, 
					function errorCallback(err){
						$ionicLoading.hide();
						if(!supressErrors){					
							$ionicPopup.alert({
								title: 'Der opstod en fejl!',
								content: 'Der opstod et problem i din forbindelse til serveren'
							});
						}
						reject();
					}
				);
			})
		},
		parseYoutubeChannel: function(url){
			return $http.get(url, 
			{ 
				params: {
					key: youtubeAPIKey,
								//playlistId: 'UCXdxZxYZV-vRjyuxjnExhiQ',
								playlistId: 'UU2m1zt6TZRZ-Bzed2TL698w', //id of the randers fc plylist
								part: 'snippet',
								maxResults: '10'
							}
						}
						);
		},
		parseMoreYoutubeChannel: function(url, pageToken){
			return $http.get(url, 
			{ 
				params: {
					key: youtubeAPIKey,
								playlistId: 'UU2m1zt6TZRZ-Bzed2TL698w', //id of the randers fc plylist
								part: 'snippet',
								maxResults: '10',
								pageToken: pageToken
							}
						}
						);
		}
	}
})

.directive('onErrorSrc', function(){
	return{
		link: function(scope, element, attrs){
			element.bind('error', function(){
				if(attrs.src != attrs.onErrorSrc){
					attrs.$set('src', attrs.onErrorSrc);
				}
			})
		}
	}
})

.directive('background', function(){
	return{
		scope: {
			sidemenu: '='
		},
		link: function(scope, element, attrs){
			element.css({'background-url': 'red'});

			// element.bind('error', function(){
			// 	if(attrs.src != attrs.onErrorSrc){
			// 		attrs.$set('src', attrs.onErrorSrc);
			// 	}
			// })
}
}
})

.directive('youtube', function($window, youTubeApiService) {
	return {
		restrict: "E",
		replace: false,
		scope: {
			videoid: "@"
		},
		template: "<div></div>",
		link: function(scope, element, attrs, $rootScope) {
			var tag = document.createElement('script');
			tag.src = "https://www.youtube.com/iframe_api";
			var firstScriptTag = document.getElementsByTagName('script')[0];
			firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

			var player;

			youTubeApiService.onReady(function() {
				player = setupPlayer(scope, element);
			});

			function setupPlayer(scope, element) {
				return new YT.Player(element.children()[0], {
					playerVars: {
						html5: 1,
						modestbranding: 1,
						iv_load_policy: 3,
						showinfo: 0,
						controls: 2,
						rel: 0
					},
					videoId: scope.videoid
				});
			};
		}
	}
})


.factory("youTubeApiService", function($q, $window) {

	var deferred = $q.defer();
	var apiReady = deferred.promise;

	$window.onYouTubeIframeAPIReady = function() {
		deferred.resolve();
	}

	return {
		onReady: function(callback) {
			apiReady.then(callback);
		}
	}   

})

.factory('UserService', function($rootScope, $ionicHistory, $location, Flash, $q, $cordovaFacebook, myStorage, FeedService, $ionicLoading, ApiEndpoint){
	return{
		logout: function(){
			myStorage.remove('currentUser');
			$rootScope.user = null;
			//remove remembered ids
			myStorage.clear();
			myStorage.remove('lastPlayerSelection');
			myStorage.remove('lastSubmittedMotmId');
			myStorage.remove('lastSubmittedSpectatorsId');
			myStorage.remove('lastSubmittedSpectatorsGuess');
			if(window.cordova){
				$cordovaFacebook.logout();
			}
			$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
			$location.path('/app/fanzone/user-login');
		},
		checkUserLogin: function(){
			var deferred = $q.defer();

			var currentUser = myStorage.getObj('currentUser');
			FeedService.post(ApiEndpoint.url+"/contestants/login/check", {token: currentUser.token})
			.then(function successCallback(res){
				$ionicLoading.hide();
				Flash.clear();
				if(res.data.valid_token){
					deferred.resolve();
				}
				else{
					deferred.reject();
				}

			}, function(error){
				$ionicLoading.hide();
				Flash.clear();
				deferred.resolve();
			});

			return deferred.promise;

		},
		checkUserForChanges: function(){
			//Create promise to return
			var deferred = $q.defer();
			
			var currentUser = myStorage.getObj('currentUser');
			if(!currentUser || !currentUser.userID){
				//User is not logged in with facebook and cant change the userdata
				deferred.reject(currentUser);
			}
			else{
				var fbUser = myStorage.getObj('currentUser');
				$cordovaFacebook.api("/me?fields=id,first_name,last_name,name,email,picture.width(720)")
				.then(function(success){
					// fbUser.gender = success.gender;
					fbUser.email = success.email;
					success.picture.data.is_silhouette ? fbUser.picture = null : fbUser.picture = success.picture.data.url;

					fbUser.name = success.name;
					fbUser.first_name = success.first_name;
					fbUser.last_name = success.last_name;

					if(JSON.stringify(fbUser) != JSON.stringify(currentUser)){
						console.log("User has changed");
						
						//Store new user
						myStorage.setObj('currentUser', fbUser);
						console.log("updating user to webservice", fbUser);

						fbUser.regid = myStorage.get('regid');
						fbUser.device = $rootScope.deviceOS;
						
						//update userdata in webservice						
						FeedService.post(ApiEndpoint.url+"/contestants/facebook/update", fbUser)
						.then(function successCallback(res){
								console.log(res);
								$ionicLoading.hide();
								Flash.clear();
								deferred.resolve(fbUser);

						}, function(error){
							$ionicLoading.hide();
							Flash.clear();
							Flash.create('danger', 'Der opstod en fejl i synkronisering af din facebook-bruger!', 5000, null, true);
							deferred.reject(fbUser);
						});
					}
					else{
						console.log("User has NOT updated");
						deferred.reject(fbUser);
					}


				}, function(error){
					deferred.reject(fbUser);
					Flash.clear();
					Flash.create('danger', 'Der opstod en fejl i forbindelsen til Facebook!', 5000, null, true);
				});

			}
		//return promise
		return deferred.promise;
		}
	}
})

.factory('myStorage', function(){
	return{
		set: function(key, value){
			window.localStorage.setItem(key, value);
		},
		get: function(key){
			return window.localStorage.getItem(key);
		},
		clear: function(){
		  	window.localStorage.clear();
		},
		remove: function(key){
			window.localStorage.removeItem(key);
		},
		getObj: function(key){
			return JSON.parse(this.get(key));
		},
		setObj: function(key, obj){
			this.set(key, JSON.stringify(obj));
		},
		removeObj: function(key){
			this.remove(key);
		}

	}
})

.factory('SocketService', function(socketFactory, myStorage){

	var activeQuiz = myStorage.getObj('activeQuiz');

	return socketFactory({
		ioSocket: io.connect('http://'+activeQuiz.quiz_server_ip+':'+activeQuiz.quiz_server_port)
	});

})

.directive('countdown', function($interval){
	return {
		restrict: 'AE',
		templateUrl: 'templates/quiz/_countdown-timerbar.html',
		link: function(scope, element, attrs){
			var endtime,
			timeoutId,
			starttime	
			function updateTimer(){
				var now = Date.now();
				!starttime ? (starttime = now, console.log(now)) : null;				

				var totalTime = (endtime - starttime);
				var passedTime = (Date.now() - starttime);

				//set timerpct
				//var pct = Math.round((passedTime / totalTime)*100);
				var pct = 100-Math.round((passedTime / totalTime)*100);

				var timerVal = Math.round((endtime-now)/1000);
				if(pct > 0 && pct < 100){
				 	scope.pct = pct;
				}
				else if(pct < 100){
				 	scope.pct = 0;
				}
				else{
				 	scope.pct = 100;
				}

				if(timerVal <= attrs.warning && timerVal > attrs.danger ){
					scope.color = 'warning';
				}
				else if(timerVal <= attrs.warning){
					scope.color = 'danger';
				}
				else{
					scope.color = '';
				}

				// //set is the text should flash
				// if(timerVal > flashFrom || timerVal <= 0){
				// 	scope.shouldFlash = false;
				// }
				// else{
				// 	scope.shouldFlash = true;
				// }

				if(timerVal > 0){
					scope.countdown = timerVal;
				}
				else{
					scope.countdown = 0;
				}
			}

			scope.$watch(attrs.endtime, function(value){
				endtime = value;
				updateTimer();
			});

			element.on('$destroy', function(){
				$interval.cancel(timeoutId);
			});

			timeoutId = $interval(function(){
				updateTimer();
			}, attrs.interval);
		}
	}
})

.factory('PushService', function($http, myStorage, ApiEndpoint, $ionicPopup, $rootScope){
	var popup = false;
	return {
		registerToken: function(data){
			$http.post(ApiEndpoint.url +'/app/notifications/register', data)
			.success(function(response){
				console.log('id sent - response: '+response);
				myStorage.set('regid', data.regid);
			})
			.error(function(response){
				console.log("error in push registration");
				console.log(response);
			});
		},
		show: function(title, message){

			var self = this;
			message = message.replace(/(?:\r\n|\r|\n)/g, '<br />');
			
			if(self.popup){
				self.popup.close();
		
				setTimeout(function() {
					self.displayMessage(title, message);
				}, 2000);

			}
			else{
				self.displayMessage(title, message);
			}

		},
		displayMessage: function(title, message){
			this.popup = $ionicPopup.alert({
				title: title,
				template: message
			});

		}

	}
})
/*.directive('dynamic', function($compile, $parse) {
		return {
			restrict: 'A',
			link: function(scope, element, attr) {
				console.log($parse(attr.dynamic)(scope));
				scope.$watch(attr.dynamic, function() {
					element.html($parse(attr.dynamic)(scope));
					$compile(element.contents())(scope);
				}, true);
			}
		}
	})
*/
/*.directive('youtube', function($window) {
	return {
		restrict: "A",
		link: function(scope, element, attrs) {
			//attrs.$set('src', attrs.src+"?rel=0&controls=1&modestbranding=1&showinfo=0");
			attrs.$set('src', attrs.src+"?rel=0&controls=1&modestbranding=1&showinfo=0");
			
		}
	}
})
*/